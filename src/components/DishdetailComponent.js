import React, { Component } from 'react';
import { Card,
		CardImg,
		CardText,
		CardBody,
		CardTitle,
		Breadcrumb, 
		Button,
		Modal,
		ModalHeader,
		ModalBody,
		Row,
		Col,
		Label,
		BreadcrumbItem
} from 'reactstrap';
import {
	Link
} from 'react-router-dom';
import {LocalForm, Control, Errors} from 'react-redux-form';
import {Loading} from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';


const RenderDish = ({dish}) => {
	return (
		<FadeTransform in
				transfromProps={{
					exitTransform: 'scale(0.5) translateY(-50%)'
		}}>
			<Card>
				<CardImg width="100%" src={baseUrl + dish.image} alt={dish.name}/>
				<CardBody>
					<CardTitle>{dish.name}</CardTitle>
					<CardText>{dish.description}</CardText>
				</CardBody>
			</Card>
		</FadeTransform>
	);
}

const RenderComments = ({comments}) => {
	var commentsList = comments.map((comment)=>{
			return (
				<Fade in>
					<li key={comment.id}>
						<blockquote className="blockquote">	
							<div className="mb-0">
								{comment.comment}
							</div>
							<footer className="blockquote-footer">
								{comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day:'2-digit'}).format(new Date(Date.parse(comment.date)))}
							</footer>
						</blockquote>
					</li>
				</Fade>
			);
		}
	);
	return (
		<ul className="list-unstyled">
			<Stagger in>
			{commentsList}
			</Stagger>
		</ul>
	);
}



const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

class CommentForm extends Component {
	constructor(props){
		super(props);
		this.handleOnSubmit = this.handleOnSubmit.bind(this);
	}

	//to Separate module
	handleOnSubmit(values){
		var postComment = this.props.postComment;
		this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
	}

	render(){
		var isOpen = this.props.isOpen;
		var toggle = this.props.toggle;
		var dishId = this.props.dishId;
		return (
			<Modal isOpen={isOpen} toggle={toggle}>
				<ModalHeader toggle={toggle}>Submit comment</ModalHeader>
				<ModalBody>
					<LocalForm onSubmit={this.handleOnSubmit} initialState={{rating: "1", yourname: "", comment: ""}}>
						<Row className="form-group">
							<Col>
								<Label htmlFor="rating">
									Rating
								</Label>
								<Control.select
									model=".rating"
									id="rating"
									className="custom-select"
									name="rating"
								>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</Control.select>
							</Col>
						</Row>
						<Row className="form-group">
							<Col>
								<Label htmlFor="author">
									Your name
								</Label>
								<Control.text
									model=".author"
									id="author"
									name="author"
									className="form-control"
									placeholder="Your name"
									validators={{
										required, 
										minLength: minLength(3),
										maxLength: maxLength(15)
									}}
								/>
								<Errors
									className="text-danger"
									model=".author"
									show="touched"
									messages={{
										requried: 'Required',
										minLength: 'Must be greater than 2 characters',
										maxLength: 'Must be 15 characters or less'
									}}
								/>
	
							</Col>
						</Row>
						<Row className="form-group">
							<Col>
								<Label htmlFor="comment">
									Comment
								</Label>
								<Control.textarea
									model=".comment"
									id="comment"
									name="comment"
									className="form-control"
									rows="6"
								/>
							</Col>
						</Row>
						<Row className="form-group">
							<Col>
								<Button type="submit" color="primary">Submit</Button>
							</Col>
						</Row>
					</LocalForm>
				</ModalBody>
			</Modal>
	)}
}


class DishDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isModalOpen: false
		};
		this.handleOnClick = this.handleOnClick.bind(this);
	}
	handleOnClick(event){
		this.setState({
			isModalOpen: !this.state.isModalOpen
		});
	}
	render(){
		var dish = this.props.dish;
		var comments = this.props.comments;
		var postComment = this.props.postComment;
		if (this.props.isLoading){
			return(
				<div className="container">
					<div className="row">
						<Loading />
					</div>
				</div>
			);
		}
		else if (this.props.errMess) {
			return(
				<div className="container">
					<div className="row">
						<h4>{this.props.errMess}</h4>
					</div>
				</div>
			);
		}
		else if (dish === null || dish === undefined) {
			return(<div className="row"></div>);
		}
		else return (
 			<div className="container">
				<div className="row">
					<Breadcrumb>
						<BreadcrumbItem>
							<Link to='/menu'>Menu</Link>
						</BreadcrumbItem>
						<BreadcrumbItem active>
							{dish.name}
						</BreadcrumbItem>
					</Breadcrumb>
					<div className="col-12">
						<h3>{dish.name}</h3>
						<hr />
					</div>
				</div>
				<div className="row">
					<div className="col-12 col-md-5 m-1">
						<RenderDish dish={dish}/>
					</div>
					<div className="col-12 col-md-5 m-1">
						<h4>Comments</h4>
						<RenderComments comments={comments}/>
						<Button onClick={this.handleOnClick} color="secondary"><span className="fa fa-pencil fa-lg"></span> Submit Comment</Button>
					</div>
					<CommentForm isOpen={this.state.isModalOpen} toggle={this.handleOnClick} postComment={postComment} dishId={dish.id}/>
				</div>
			</div>
		);	
	}
}

export default DishDetail;
