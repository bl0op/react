import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Dishes } from './dishes';
import { createForms } from 'react-redux-form';
import { Comments } from './comments';
import { Promotions } from './promotions';
import { Leaders } from './leaders';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { initialFeedback } from './forms';


export const ConfigureStore = () => {
	const store = createStore(
		combineReducers({
			dishes: Dishes,
			comments: Comments,
			promotions: Promotions,
			leaders: Leaders,
			...createForms({
				feedback: initialFeedback
			})
		}), 
		applyMiddleware(thunk, logger)
		//,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	);

	return store;
}
